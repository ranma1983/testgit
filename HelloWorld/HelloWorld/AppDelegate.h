//
//  AppDelegate.h
//  HelloWorld
//
//  Created by hudan@l2cplat.com on 15/5/5.
//  Copyright (c) 2015年 l2cplat.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


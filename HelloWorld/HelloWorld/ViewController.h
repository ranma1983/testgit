//
//  ViewController.h
//  HelloWorld
//
//  Created by hudan@l2cplat.com on 15/5/5.
//  Copyright (c) 2015年 l2cplat.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

- (IBAction)changeGreeting:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (copy, nonatomic) NSString *userName;
@end


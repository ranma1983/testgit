//
//  main.m
//  HelloWorld
//
//  Created by hudan@l2cplat.com on 15/5/5.
//  Copyright (c) 2015年 l2cplat.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
